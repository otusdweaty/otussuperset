# SUPERSET-OTUS

#### Установка платформы на YandexCloud:
1) Ввести переменные при запуске pipline(ОБЯЗАТЕЛЬНО!!!):

Параметры YandexCloud:
| Name Variable                     | Describe                                          |  Type  |
|-----------------------------------|---------------------------------------------------|--------|
| CI_YANDEX_CLOUD_ID                | Идентификатор вашего облака                       | string |
| CI_YANDEX_FOLDER_ID               | Идентификатора папки                              | string |
| CI_YANDEX_COM_DEF_ZONE            | Имя вычислительной зоны                           | string |
| CI_YANDEX_TOKEN                   | Токен вашего аккаунта                             | string |
| CI_EMAIL_ADDRESS                  | Адрес почты                                       | string |


Параметры установки SUPERSET:
| Name Variable                     | Describe                                                        |  Type  |     
|-----------------------------------|-----------------------------------------------------------------|--------|
| CI_REGISTRY_HARBOR                | Ссылка на существующий registy                                  | string |
| CI_REGISTRY_HARBOR_USER           | Логин от вашего registry                                        | string |
| CI_REGISTRY_HARBOR_PASSWORD       | Пароль от вашего registry                                       | string |
| CI_REGISTRY_USER                  | Логин от docker hub                                             | string |
| CI_REGISTRY_PASSWORD              | Пароль от docker hub                                            | string |

2) Запустить pipline
3) Ожидать завершение установка ~ 60-80 минут
